/** A class to record some information about a car */
class Car
{
	private int speed;
	/** Get the position of the car */
	private int position;
	private int act;
	private int dv;
	
	public void setPosition(int f){position=f;}
	public int getPosition(){return position;}
	
	/** Make the car move */
	public int move()
	{
		position+=speed;
		return position;
	}
	/** Adjust the speed of the car */
	public void adjust(int front)
	{
		if(front==-1)
		{
			act=1;
			dv=4;
			speed=2;
			return;
		}
		if(act==2)
		{
			act--;
		}
		else if(act==1)
		{
			act--;
			speed=dv;
		}
		else
		{
			int dis=(front-position)/2;
			if(dis>speed)
			{
				act=2;
				dv=dis;
			}
			else if(dis<speed)
			{
				act=1;
				dv=dis;
			}
		}
		if(speed>4)speed=4;//System.out.print(speed);
	}
	/** Initialize a car */
	public Car(int pos,int front)
	{
		position=pos;
		speed=(front-position)/2;
		if(speed>4)speed=4;
		act=0;
		
	}
}
/** A class to record the current state of the highway */
class Highway
{
	private int len;
	public int[] road;
	
	public int[] getRoad()
	{
		return road;
	}
	
	/** Remove all the cars */
	public void clean()
	{
		for(int i=1;i<=len;i++)
			road[i]=0;
	}
	/** Add a car onto the highway */
	public void add(int pos)
	{
		road[pos]=1;
	}
	
	/** Initialize the highway */
	public Highway(int l)
	{
		len=l;
		road=new int[len+100];
	}
}
/** A class to simulate the highway */
class CarSimulation
{
	
	
	public int task,len,begin,inter;
	public Car[] begcar,intcar;
	public Highway Hw;
	
	public int bega,begb,inta,intb;
	public int k=0;
	public CarSimulation(int t,int l,int b,int i)
	{
		task=t;
		len=l;
		begin=b;
		inter=i;
		begcar=new Car[250];
		intcar=new Car[250];
		Hw=new Highway(len);
		bega=0;begb=0;
		inta=0;intb=0;
	}
	
	public int[] getRoad()
	{
		Hw.clean();
		for(int i=bega;i<begb;i++)
			Hw.add(begcar[i].getPosition());
		for(int i=inta;i<intb;i++)
			Hw.add(intcar[i].getPosition());
		return Hw.getRoad();
	}
	
	public int oneStep()
	{
		int intclose=20000;
		int begclose=20000;
		
		for(int i=bega;i<begb;i++)
		{
			Hw.add(begcar[i].getPosition());
			if(begcar[i].getPosition()<begclose)
				begclose=begcar[i].getPosition();
			if(begcar[i].getPosition()<intclose&&begcar[i].getPosition()>=50)
				intclose=begcar[i].getPosition();
			
		}
		for(int i=inta;i<intb;i++)
		{
			Hw.add(intcar[i].getPosition());
			if(intcar[i].getPosition()<begclose)
				begclose=intcar[i].getPosition();
			if(intcar[i].getPosition()<intclose&&intcar[i].getPosition()>=50)
				intclose=intcar[i].getPosition();
		}
		
		if(begb<begin&&begclose>=3)
		{
			begcar[begb]=new Car(1,begclose);
			begb++;
		}
		if(task==3&&intb<inter&&intclose>=52)
		{
			intcar[intb]=new Car(50,intclose);
			intb++;
		}
		int sum=begb-bega+intb-inta;
		int begn=bega,intn=inta;
		int front=20000;
		for(int i=0;i<sum;i++)
		{
			int temp=-1;
			if(begn<begb)temp=1;
			if(temp==1&&intn<intb)
				if(intcar[intn].getPosition()>begcar[begn].getPosition())
					temp=-1;
			if(temp==-1)
			{
				intcar[intn].adjust(front);
				front=intcar[intn].getPosition();
				intn++;
			}
			else
			{
				begcar[begn].adjust(front);
				front=begcar[begn].getPosition();
				begn++;
			}
		}
		if(k==5&&task==2)
			begcar[0].adjust(-1);
		int end=-1;
		begn=bega;
		intn=inta;
		front=20000;
		for(int i=0;i<sum;i++)
		{
			int temp=-1;
			if(begn<begb)temp=1;
			if(temp==1&&intn<intb)
				if(intcar[intn].getPosition()>begcar[begn].getPosition())
					temp=-1;
			if(temp==-1)
			{
				intcar[intn].move();
				if(intcar[intn].getPosition()>=front)
				{
					end=front;
					intcar[intn].setPosition(front);
				}
				else
					front=intcar[intn].getPosition();
				if(intcar[intn].getPosition()>len)inta++;
				intn++;
				
			}
			else
			{
				begcar[begn].move();
				if(begcar[begn].getPosition()>=front)
				{
					end=front;
					begcar[begn].setPosition(front);
				}
				else
					front=begcar[begn].getPosition();
				if(begcar[begn].getPosition()>len)bega++;
				begn++;
				
			}
		}
		k++;
		return end;
	}
}