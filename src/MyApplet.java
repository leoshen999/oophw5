import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.applet.*;


public class MyApplet extends JApplet
{
	public int task=1;
	XDThread xdthread;
	JTextField tf1,tf2,tf3;
	JLabel mess;
	JButton btn;
	public void init()
	{
		setSize(848,145);
		setLayout(null);
		
		xdthread=new XDThread(this);
		
		//////////////// Add CheckBox 1~3
		Checkbox cb1,cb2,cb3;
		CheckboxGroup cbg=new CheckboxGroup();
		cb1=new Checkbox("Task 1",true);
		cb2=new Checkbox("Task 2",false);
		cb3=new Checkbox("Task 3",false);
		cb1.setCheckboxGroup(cbg);
		cb2.setCheckboxGroup(cbg);
		cb3.setCheckboxGroup(cbg);
		cb1.addItemListener(new XDItemListener(this));
		cb2.addItemListener(new XDItemListener(this));
		cb3.addItemListener(new XDItemListener(this));
		cb1.setBounds(170,56,60,20);
		cb2.setBounds(170,86,60,20);
		cb3.setBounds(170,116,60,20);
		
		add(cb1);
		add(cb2);
		add(cb3);
		////////////////////////////////
		
		
		//////////////// Add TextField
		tf1=new JTextField();
		tf2=new JTextField();
		tf3=new JTextField();
		tf1.setBounds(310,56,80,20);
		tf2.setBounds(310,86,80,20);
		tf3.setBounds(310,116,80,20);
		add(tf1);
		add(tf2);
		add(tf3);
		////////////////////////////////
		
		//////////////// Add Button
		btn=new JButton("Run");
		btn.addActionListener(new XDActionListener(xdthread));
		btn.setBounds(400,56,80,80);
		add(btn);
		////////////////////////////////
		
		//////////////// Add Label
		JLabel lbl1,lbl2,lbl3;
		lbl1=new JLabel("      Length");
		lbl2=new JLabel("   Beginning");
		lbl3=new JLabel("Intersection");
		lbl1.setBounds(240,56,80,20);
		lbl2.setBounds(240,86,80,20);
		lbl3.setBounds(240,116,80,20);
		add(lbl1);
		add(lbl2);
		add(lbl3);
		
		mess=new JLabel("Status: Ready to Run!");
		mess.setBounds(500,100,400,20);
		add(mess);
		////////////////////////////////
		
		xdthread.start();
	}/*
	public void start()
	{
		
	}*/
	
}
class XDActionListener implements ActionListener
{
	private Thread target;
	public XDActionListener(Thread tar)
	{
		target=tar;
	}
	public void actionPerformed(ActionEvent e)
	{
		target.interrupt();
	}
}
class XDItemListener implements ItemListener
{
	private MyApplet ma;
	public XDItemListener(MyApplet m)
	{
		ma=m;
	}
	public void itemStateChanged(ItemEvent e)
	{
		String str=(String)e.getItem();
		if(str.equals("Task 1"))
			ma.task=1;
		else if(str.equals("Task 2"))
			ma.task=2;
		else
			ma.task=3;
		System.out.println(str);
	}
}
class XDThread extends Thread
{
	public ImageIcon img,img2;
	public JLabel[] car;
	
	public MyApplet app;
	
	public int length;
	public int inter;
	public int begin;
	
	public CarSimulation CS;
	
	public XDThread(MyApplet a)
	{
		app=a;
	}
	public void run()
	{
		img = new ImageIcon( app.getImage(app.getCodeBase(),"./car_s.png") );
		img2 = new ImageIcon( app.getImage(app.getCodeBase(),"./wrong.png") );
		car=new JLabel[100];
		for(int i=0;i<100;i++)
		{
			car[i]=new JLabel();
			car[i].setIcon(img);
		}
		while(true)
		{
			app.btn.setText("Start");
			while(true)
			{
				try
				{
					Thread.sleep(999999999);
				}
				catch(InterruptedException e)
				{
					try
					{
						length=Integer.parseInt(app.tf1.getText());
						begin=Integer.parseInt(app.tf2.getText());
						if(app.task==3)inter=Integer.parseInt(app.tf3.getText());
						else inter=0;
						CS=new CarSimulation(app.task,length,begin,inter);
						break;
					}
					catch(NumberFormatException e2)
					{
						app.mess.setText("Status: Input format error, please retry.");
					}
				}
			}
			int check=-1;
			app.btn.setText("Stop");
			for(int k=0;k<250;k++)
			{
				try
				{
					if(check!=-1)app.mess.setText("Status: Round "+(k+1)+" Accident Occur!!");
					else app.mess.setText("Status: Round "+(k+1)+" Running...");
					int[] road=CS.getRoad();
					for(int i=0;i<100;i++)
						app.remove(car[i]);
					int j=0;
					int tmp=road.length;
					if(tmp>=400)tmp=399;
					for(int i=0;i<tmp;i++)
						if(road[i]==1)
						{
							if(i==check)
								car[j].setIcon(img2);
							else
								car[j].setIcon(img);
							car[j].setBounds(20+i*2,20,16,16);
							app.add(car[j]);
							j++;
						}
					app.repaint();
					if(check!=-1)break;
					Thread.sleep(25);
					check=CS.oneStep();
				}
				catch(InterruptedException e)
				{
					break;
				}
			}
			if(check==-1)app.mess.setText("Status: Simulation End.");
		}
	}
}